package com.dost.sil.aquosense;



import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Silerio on 11/1/2017.
 */

public class DBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "MyDBName.db";
    public static final String PARAMETER_TABLE = "parameter_logs";
    public static final String PARAMETER_ID = "id";
    public static final String PARAMETER_DEVICENAME = "devicename";
    public static final String PARAMETER_DATE = "dtime";
    public static final String PARAMETER_GPS = "gps";
    public static final String PARAMETER_TEMP = "temp";
    public static final String PARAMETER_PH = "ph";
    public static final String PARAMETER_DO = "do";
    public static final String PARAMETER_EC = "ec";
    public static final String PARAMETER_SAL = "sal";
    public static final String PARAMETER_TDS = "tds";
    public static final String PARAMETER_SG = "sg";
    public static final String PARAMETER_AMM = "amm";
    private HashMap hp;



    public DBHelper(Context context) {
        //super(context, DATABASE_NAME , null, 1);
        super(context, context.getExternalFilesDir(null).getAbsolutePath() + "/" + DATABASE_NAME, null, 1);


    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        db.execSQL(
                "create table parameter_logs " +
                        "(id integer primary key, devicename text, dtime datetime,gps text, temp text, ph text,do text, ec text, sal text, tds text, sg text, amm text)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        db.execSQL("DROP TABLE IF EXISTS parameter_logs");
        onCreate(db);
    }

    public boolean insertParameter (String devicename, String date, String gps, String temp, String ph, String dox, String ec,  String sal, String tds, String sg, String amm) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("devicename", devicename);
        contentValues.put("dtime", date);
        contentValues.put("gps", gps);
        contentValues.put("temp", temp);
        contentValues.put("ph", ph);
        contentValues.put("do", dox);
        contentValues.put("ec", ec);
        contentValues.put("sal", sal);
        contentValues.put("tds", tds);
        contentValues.put("sg", sg);
        contentValues.put("amm", amm);
        db.insert("parameter_logs", null, contentValues);
        return true;
    }

    public Cursor getData(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from parameter_logs where id="+ id +"", null );
        return res;
    }

    public int numberOfRows(){
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, PARAMETER_TABLE);
        return numRows;
    }

    public boolean updateParamDetails (Integer id, String name, String phone, String email, String street,String place) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", name);
        contentValues.put("phone", phone);
        contentValues.put("email", email);
        contentValues.put("street", street);
        contentValues.put("place", place);
        db.update("parameter_logs", contentValues, "id = ? ", new String[] { Integer.toString(id) } );
        return true;
    }

    public Integer deleteLog (Integer id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete("parameter_logs",
                "id = ? ",
                new String[] { Integer.toString(id) });
    }


    /* return all id */
    public ArrayList<String> getAllParameterLogs() {
        ArrayList<String> array_list = new ArrayList<String>();

        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from  parameter_logs", null );
        res.moveToFirst();

        while(res.isAfterLast() == false){
            res.getString(0);
            array_list.add(res.getString(res.getColumnIndex(PARAMETER_ID))); //will change to date

            res.moveToNext();
        }
        return array_list;
    }

    public boolean checkIfDateExists(String date){

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select dtime from parameter_logs where dtime = '" + date + "'", null );
        res.moveToFirst();

        while(res.isAfterLast() == false){
            return true;
        }
        return false;

    }




}
