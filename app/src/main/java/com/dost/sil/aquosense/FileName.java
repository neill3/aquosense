package com.dost.sil.aquosense;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Silerio on 1/25/2018.
 */

public class FileName {

    private String [] filenames;

    private static FileName instance = new FileName();

    private FileName(){
    }

    public static FileName getInstance (){
        return instance;
    }

    public String[] getFilenames() {
        return filenames;
    }

    public void setFilenames(String[] filenames) {
        this.filenames = filenames;
    }

    public void parseFileNames(String serialData, int startOfSet, int endOfSet){


        String[] lines =  serialData.substring(startOfSet, endOfSet).replace("_",":").split(";");
        java.util.List<String> list = new ArrayList<>();
        Collections.addAll(list, lines);
        list.remove(0);
        list.remove(0);
        list.remove((lines.length - 3));
        lines = list.toArray(new String[0]);
        this.filenames = lines;


    }
}
