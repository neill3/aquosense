package com.dost.sil.aquosense;

/**
 * Created by Silerio on 1/26/2018.
 */

class ModeFetch implements ModeState {
    
    private final Mode mode;
    private boolean modeStat;


    public ModeFetch(Mode mode) {
        this.mode = mode;
    }

    @Override
    public boolean modeStat() {
        return this.modeStat;
    }

    @Override
    public void list() {
        mode.setState(mode.getList());
        mode.list();
    }

    @Override
    public void read() {
        mode.setState(mode.getRead());
        mode.read();
    }

    @Override
    public void calibrate() {

    }

    @Override
    public void fetch() {

        // added 1/2/2018
        try {
           // SerialData sd = SerialData.getInstance();
           // String dataSet = sd.getSerialData();

            //  Hard code dataSet
             String dataSet =  "XGETD;DNAME=AQST0001;DATE= 2017-11-02 13_12_09;LAT=1438.3720,N;LONG=12104.6145,E;TEMP=32.45;PH=5.45;DO=10000;EC=23000;SAL=1.00;TDS=2.10;SG=1.56;AMM=0.13;+57:";

            int startDataSet = 0;

            if (dataSet.contains("XGETD;")) {
                startDataSet = dataSet.lastIndexOf("XGETD;");
            } else {
                this.modeStat = false;
            }

            int endDataSet = dataSet.lastIndexOf(":");
            int checkSumDataSet = dataSet.lastIndexOf("+");

            if (startDataSet < endDataSet) {

                char[] serialChar = null;

                try {
                    serialChar = dataSet.substring(startDataSet, checkSumDataSet).toCharArray();
                } catch (Exception e) {

                }

                // get current length of the dataSet
                int dSetLength = serialChar.length;
                int calculatedSum = 0;

                for (int counter = 0; counter < dSetLength; counter++) {

                    calculatedSum ^= serialChar[counter];

                } // end for Loop (int counter = 0; counter <chkSumDataSet ; counter++)


                //get the number of calculatedSum
                //get the provided Sum in the DataSet
                //compare data and send response

                String sumInSet = dataSet.substring(checkSumDataSet, endDataSet).replaceAll("[^0-9.]", "");

                int number;
                try {
                    number = Integer.parseInt(sumInSet);

                } catch (Exception e) {
                    number = 0;
                }
                int sumOfSet = number;

                if (sumOfSet == calculatedSum) {
                    // sendSerialData to duino
                    // sendSerialtoBluno("RSTAT,DEVNAME,RETURN STATUS;");
                    //  parseSerialData(dataSet ,startDataSet, endDataSet);

                    Fetch fetch = Fetch.getInstance();
                    fetch.parseSerialData(dataSet, startDataSet, endDataSet);

                    this.modeStat = true;


                } else {
                    // sendSerialtoBluno("RSTAT,DEVNAME,RETURN STATUS;");
                    // hmapSerial.put("devicename", "error");
                    // SERIALDATA = "";

                    this.modeStat = false;
                }

            } // end if

        }
        catch(Exception e){

        }




    }

    @Override
    public void files() {
        mode.setState(mode.getFiles());
        mode.files();

    }
}
