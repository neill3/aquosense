package com.dost.sil.aquosense;

/**
 * Created by Silerio on 12/3/2017.
 */

public class Mode implements ModeState{

    private ModeState list;
    private ModeState read;
    private ModeState calib;
    private ModeState fetch;
    private ModeState files;


    private ModeState state;

    // constructor
    public Mode(){
        this.list = new ModeList(this);
        this.read = new ModeRead(this);
        this.files = new ModeFiles(this);
        this.fetch = new ModeFetch(this);
        this.state = list;
    }

    public ModeState getFiles() {
        return files;
    }

    public void setFiles(ModeState files) {
        this.files = files;
    }

    public void setState(ModeState state){
        this.state = state;
    }

    public ModeState getState(){
        return state;
    }


    public ModeState getList() {
        return list;
    }

    public void setList(ModeState list) {
        this.list = list;
    }

    public ModeState getRead() {
        return read;
    }

    public void setRead(ModeState read) {
        this.read = read;
    }

    public ModeState getCalib() {
        return calib;
    }

    public void setCalib(ModeState calib) {
        this.calib = calib;
    }

    public ModeState getFetch() {
        return fetch;
    }

    public void setFetch(ModeState fetch) {
        this.fetch = fetch;
    }


    @Override
    public boolean modeStat() {
        return state.modeStat();
    }

    @Override
    public void list() {
        state.list();
    }

    @Override
    public void read() {
        state.read();
    }

    @Override
    public void calibrate() {

    }

    @Override
    public void fetch() {
        state.fetch();
    }

    @Override
    public void files() {
        state.files();
    }
}
