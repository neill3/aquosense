package com.dost.sil.aquosense;

import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbManager;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.felhr.usbserial.UsbSerialDevice;
import com.felhr.usbserial.UsbSerialInterface;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Silerio on 10/25/2017.
 */

public class UsbSerialService extends Service {


    private boolean connectionStatus;

    private final IBinder mBinder = new MyBinder();
    Intent usbDisabled, usbEnabled;

    private final String TAG = "UsbSerialService";
    public  final String ACTION_USB_PERMISSION = "com.example.silerio.aquosense.USB_PERMISSION";
    private static String SERIALDATA = "\n";
    private static boolean RUNTHREAD = false;

    UsbManager usbManager;
    UsbDevice device;
    UsbSerialDevice serialPort;
    UsbDeviceConnection connection;

    public static HashMap <String, String> hmapSerial;
    Thread t1;



    // triggers when data is sent
    UsbSerialInterface.UsbReadCallback mCallback = new UsbSerialInterface.UsbReadCallback() {
        @Override
        public void onReceivedData(byte[] arg0) {
            String data = null;
            try {

                data = new String(arg0, "UTF-8");

                SERIALDATA = new StringBuilder().append(SERIALDATA).append(data).toString();
                setSerialData(SERIALDATA);


            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        }
    }; // end usbSerial callback


    // usb BroadCastReceiver
    private final BroadcastReceiver broadcastReceiver = new BroadcastReceiver() { //Broadcast Receiver to automatically start and stop the Serial connection.
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(ACTION_USB_PERMISSION)) {
                boolean granted = intent.getExtras().getBoolean(UsbManager.EXTRA_PERMISSION_GRANTED);
                if (granted) {
                    connection = usbManager.openDevice(device);
                    serialPort = UsbSerialDevice.createUsbSerialDevice(device, connection);
                    if (serialPort != null) {
                        if (serialPort.open()) { //Set Serial Connection Parameters.
                            connectionStatus = true;

                            sendBroadcast(usbEnabled);

                            serialPort.setBaudRate(9600);
                            serialPort.setDataBits(UsbSerialInterface.DATA_BITS_8);
                            serialPort.setStopBits(UsbSerialInterface.STOP_BITS_1);
                            serialPort.setParity(UsbSerialInterface.PARITY_NONE);
                            serialPort.setFlowControl(UsbSerialInterface.FLOW_CONTROL_OFF);
                            serialPort.read(mCallback);

                        } else {
                            Log.d("SERIAL", "PORT NOT OPEN");
                            sendBroadcast(usbDisabled);
                        }
                    } else {
                        Log.d("SERIAL", "PORT IS NULL");
                        sendBroadcast(usbDisabled);
                    }
                } else {
                    Log.d("SERIAL", "PERM NOT GRANTED");
                    sendBroadcast(usbDisabled);
                }
            } else if (intent.getAction().equals(UsbManager.ACTION_USB_DEVICE_ATTACHED)) {
                //
            } else if (intent.getAction().equals(UsbManager.ACTION_USB_DEVICE_DETACHED)) {

                 serialPort.close();
                 sendBroadcast(usbDisabled);

            }
        }
    };  // end broadcast receiver



    private static int countLines(String str){
        String[] lines = str.split("\r\n|\r|\n");
        return  lines.length;
    }


    @Override
    public void onCreate() {
        connectionStatus = false;
        usbManager = (UsbManager) getSystemService(this.USB_SERVICE);

        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_USB_PERMISSION);
        filter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);
        filter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);
        registerReceiver(broadcastReceiver, filter);

        hmapSerial = new HashMap<String, String>();

        hmapSerial.put("devicename", "n/a");
        // hmapSerial.put("gps", "0.00");
        hmapSerial.put("date", "n/a");
        hmapSerial.put("temp","0.00");
        hmapSerial.put("ph","0.00");
        hmapSerial.put("do","0.00");
        hmapSerial.put("ec","0.00");
        hmapSerial.put("sal","0.00");
        hmapSerial.put("tds","0.00");
        hmapSerial.put("sg","0.00");
        hmapSerial.put("amm","0.00");

        usbDisabled = new Intent("UsbSerialService.ACTION_USB_DISABLED");
        usbEnabled = new Intent ("UsbSerialService.ACTION_USB_ENABLED");

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }


    public void setUsbConnection(){

        HashMap<String, UsbDevice> usbDevices = usbManager.getDeviceList();
        if (!usbDevices.isEmpty()) {
            boolean keep = true;
            for (Map.Entry<String, UsbDevice> entry : usbDevices.entrySet()) {
                device = entry.getValue();
                int deviceVID = device.getVendorId();
                if (deviceVID == 0x2341)//Arduino Vendor ID
                {
                    PendingIntent pi = PendingIntent.getBroadcast(this, 0, new Intent(ACTION_USB_PERMISSION), 0);
                    usbManager.requestPermission(device, pi);
                   // RUNTHREAD = true;
                    keep = false;

                } else {
                    connection = null;
                    device = null;
                }

                if (!keep)
                    break;
            }
        }else{
            sendBroadcast(usbDisabled);
        }

        //create thread
      //  t1 = new Thread(new SerialStringData(), "t1");
      //  t1.start();
    }



    @Override
    public void onDestroy() {
       // RUNTHREAD = false;
       // serialPort.close();

        super.onDestroy();
        unregisterReceiver(broadcastReceiver);

    }


    public void sendSerialtoBluno(String message){
        serialPort.write(message.getBytes());
    }

    public void stopSerialReading(){

        try {

            if (connectionStatus == true) {
                sendSerialtoBluno("STOP:");
                serialPort.close();
         //       Toast.makeText(this, "Connection status was true" , Toast.LENGTH_SHORT).show();
            }

            SERIALDATA = "\n";
       //     Toast.makeText(this, "Stop serial reading", Toast.LENGTH_SHORT).show();
        }catch(Exception e){
            Toast.makeText(this, e.getMessage().toString(), Toast.LENGTH_SHORT).show();
        }
    }



    synchronized public void setSerialData(String data){

        SerialData sd = SerialData.getInstance();
        sd.setSerialData(data);
    }

    synchronized public void clearSerialData(){
        SERIALDATA = "\n";
        setSerialData(" ");
    }


    //  Class used for the client Binder.
    public class MyBinder extends Binder {
        UsbSerialService getService(){
            return UsbSerialService.this;
        }
    }



}
