package com.dost.sil.aquosense;

/**
 * Created by Silerio on 12/3/2017.
 */

public class ModeList implements ModeState {

    private final Mode mode;
    private boolean modeStat;

    public ModeList(Mode mode) {
        this.mode = mode;
    }


    @Override
    public boolean modeStat() {
        return this.modeStat;
    }

    @Override
    public void list() {
       // List l = List.getInstance();
       // l.setDevices(new String[] {"hua", "hasfa", "hhfas"});

        // added 1/2/2018
        try {
           // SerialData sd = SerialData.getInstance();
           // String dataSet = sd.getSerialData();

             //Hard code dataSet
            String dataSet = "XNAME;AQS01AAA; AQS02BBB; AQS02CCC; AQS03DDD; AQS02EEE; AQS02FFF; AQS02GGG;+92:";

            int startDataSet = 0;

            if (dataSet.contains("XNAME;")) {
                startDataSet = dataSet.lastIndexOf("XNAME;");
            } else {
                this.modeStat = false;
            }

            int endDataSet = dataSet.lastIndexOf(":");
            int checkSumDataSet = dataSet.lastIndexOf("+");

            if (startDataSet < endDataSet) {

                char[] serialChar = null;

                try {
                    serialChar = dataSet.substring(startDataSet, checkSumDataSet).toCharArray();
                } catch (Exception e) {

                }

                // get current length of the dataSet
                int dSetLength = serialChar.length;
                int calculatedSum = 0;

                for (int counter = 0; counter < dSetLength; counter++) {

                    calculatedSum ^= serialChar[counter];

                } // end for Loop (int counter = 0; counter <chkSumDataSet ; counter++)


                //get the number of calculatedSum
                //get the provided Sum in the DataSet
                //compare data and send response

                String sumInSet = dataSet.substring(checkSumDataSet, endDataSet).replaceAll("[^0-9.]", "");

                int number;
                try {
                    number = Integer.parseInt(sumInSet);

                } catch (Exception e) {
                    number = 0;
                }
                int sumOfSet = number;

                if (sumOfSet == calculatedSum) {
                    // sendSerialData to duino
                    // sendSerialtoBluno("RSTAT,DEVNAME,RETURN STATUS;");
                    //  parseSerialData(dataSet ,startDataSet, endDataSet);

                    List myLi = List.getInstance();
                    myLi.setDevices(dataSet, startDataSet, endDataSet);

                    this.modeStat = true;


                } else {
                    // sendSerialtoBluno("RSTAT,DEVNAME,RETURN STATUS;");
                    // hmapSerial.put("devicename", "error");
                    // SERIALDATA = "";

                    this.modeStat = false;
                }

            } // end if

        }
        catch(Exception e){

        }

    }

    @Override
    public void read() {
       mode.setState(mode.getRead());
       mode.read();

    }

    @Override
    public void calibrate() {

    }

    @Override
    public void fetch() {
        mode.setState(mode.getFetch());
        mode.fetch();

    }

    @Override
    public void files() {
        mode.setState(mode.getFiles());
        mode.files();
    }
}
