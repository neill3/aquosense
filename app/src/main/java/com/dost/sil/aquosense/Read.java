package com.dost.sil.aquosense;

/**
 * Created by Silerio on 12/3/2017.
 */

public class Read {

    private String devicename;
    private String date;
    private String gps;
    private String temp;
    private String ph;
    private String dOx;
    private String ec;
    private String sal;
    private String tds;
    private String sg;
    private String amm;

    private static Read instance = new Read();

    private Read(){
        this.devicename = " ";
        this.date       = " ";
        this.gps        = " ";
        this.temp       = " ";
        this.ph         = " ";
        this.dOx        = " ";
        this.ec         = " ";
        this.sal        = " ";
        this.tds        = " ";
        this.sg         = " ";
        this.amm        = " ";
    }

    public static Read getInstance(){
        return instance;
    }

    public void clear(){
        this.devicename = " ";
        this.date       = " ";
        this.gps        = " ";
        this.temp       = " ";
        this.ph         = " ";
        this.dOx        = " ";
        this.ec         = " ";
        this.sal        = " ";
        this.tds        = " ";
        this.sg         = " ";
        this.amm        = " ";

    }


    public void parseSerialData(String serialData, int startOfSet, int endOfSet){
        clear();
        String[] lines =  serialData.substring(startOfSet, endOfSet).split(";");

        for (int x = 0; x < lines.length; x++) {
            String lineData = lines[x];

                if (lineData.toLowerCase().contains("dname")) {
                    lineData = lineData.replace("DNAME=", "");
                    setDevicename(lineData);
                } else if (lineData.toLowerCase().contains("date")) {
                    setDate(getValue(lineData).replace("_", ":"));
                } else if (lineData.toLowerCase().contains("temp")) {
                    setTemp(getValue(lineData));
                } else if (lineData.toLowerCase().contains("ph")) {
                    setPh(getValue(lineData));
                } else if (lineData.toLowerCase().contains("do")) {
                    setdOx(getValue(lineData));
                } else if (lineData.toLowerCase().contains("ec")) {
                    setEc(getValue(lineData));
                } else if (lineData.toLowerCase().contains("sal")) {
                    setSal(getValue(lineData));
                } else if (lineData.toLowerCase().contains("tds")) {
                    setTds(getValue(lineData));
                } else if (lineData.toLowerCase().contains("sg")) {
                    setSg(getValue(lineData));
                } else if (lineData.toLowerCase().contains("amm")) {
                    setAmm(getValue(lineData));
                }
            }// end for loop


    }

    private String getValue(String value){

        return value.replaceAll("[^\\d.]", "");
    } // end getValue


    public String getDevicename() {
        return devicename;
    }

    public void setDevicename(String devicename) {
        this.devicename = devicename;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getGps() {
        return gps;
    }

    public void setGps(String gps) {
        this.gps = gps;
    }

    public String getTemp() {
        return temp;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }

    public String getPh() {
        return this.ph;
    }

    public void setPh(String ph) {
        this.ph = ph;
    }

    public String getdOx() {
        return dOx;
    }

    public void setdOx(String dOx) {
        this.dOx = dOx;
    }

    public String getEc() {
        return ec;
    }

    public void setEc(String ec) {
        this.ec = ec;
    }

    public String getSal() {
        return sal;
    }

    public void setSal(String sal) {
        this.sal = sal;
    }

    public String getTds() {
        return tds;
    }

    public void setTds(String tds) {
        this.tds = tds;
    }

    public String getSg() {
        return sg;
    }

    public void setSg(String sg) {
        this.sg = sg;
    }

    public String getAmm() {
        return amm;
    }

    public void setAmm(String amm) {
        this.amm = amm;
    }
}
