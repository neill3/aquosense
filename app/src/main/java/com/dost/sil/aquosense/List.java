package com.dost.sil.aquosense;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Silerio on 12/3/2017.
 */

public class List{

    private String [] devices;


    private static List listObject = new List();

    // private constructor
    private List(){
    }

    public static List getInstance(){
        return listObject;
    }

    public String[] getDevices() {
        return devices;
    }


    public void setDevices(String serialData, int startOfSet, int endOfSet) {
        String[] lines =  serialData.substring(startOfSet, endOfSet).split(";");
        java.util.List<String> list = new ArrayList<>();
        Collections.addAll(list, lines);
        list.remove(0);
        list.remove((lines.length - 2));
        lines = list.toArray(new String[0]);

        this.devices = lines;
    }

}
