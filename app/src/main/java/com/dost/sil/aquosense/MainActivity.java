package com.dost.sil.aquosense;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    Button send_button, logs_button;

    Button send_buttonagain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Button Scan
        send_button = (Button) findViewById(R.id.button_scan);
        send_button.setOnTouchListener(new View.OnTouchListener() {
            //  @Override
            public boolean onTouch(View view, MotionEvent event) {
                // Toast.makeText(getApplicationContext(), "initial",Toast.LENGTH_SHORT).show();
                switch (event.getActionMasked()) {
                    case MotionEvent.ACTION_DOWN:
                        view.setPressed(true);
                        //   Toast.makeText(getApplicationContext(), "pressed",Toast.LENGTH_SHORT).show();
                        readParameters();
                        break;
                    case MotionEvent.ACTION_UP:
                    case MotionEvent.ACTION_OUTSIDE:
                    case MotionEvent.ACTION_CANCEL:
                        view.setPressed(false);
                        // Stop action ...
                        break;
                    case MotionEvent.ACTION_POINTER_DOWN:
                        break;
                    case MotionEvent.ACTION_POINTER_UP:
                        break;
                    case MotionEvent.ACTION_MOVE:
                        break;


                }
                return false; //we return false so that the click listener will process the event
            }
        });



      /* Calibrate button
        Button calib = (Button) findViewById(R.id.button_calibrate);
        calib.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            }
        });

        */


        logs_button = (Button) findViewById(R.id.button_logs);
        logs_button.setOnTouchListener(new View.OnTouchListener() {
            //  @Override
            public boolean onTouch(View view, MotionEvent event) {
                // Toast.makeText(getApplicationContext(), "initial",Toast.LENGTH_SHORT).show();
                switch (event.getActionMasked()) {
                    case MotionEvent.ACTION_DOWN:
                        view.setPressed(true);
                        //   Toast.makeText(getApplicationContext(), "pressed",Toast.LENGTH_SHORT).show();
                        viewLogs();
                        break;
                    case MotionEvent.ACTION_UP:
                    case MotionEvent.ACTION_OUTSIDE:
                    case MotionEvent.ACTION_CANCEL:
                        view.setPressed(false);
                        // Stop action ...
                        break;
                    case MotionEvent.ACTION_POINTER_DOWN:
                        break;
                    case MotionEvent.ACTION_POINTER_UP:
                        break;
                    case MotionEvent.ACTION_MOVE:
                        break;


                }
                return false; //we return false so that the click listener will process the event
            }
        });



    }



    private void readParameters(){
           Intent intent = new Intent(this, ReadParameters.class);
           startActivity(intent);

    }

    private void  viewLogs(){
        Intent intent = new Intent(this, ViewLogs.class);
        startActivity(intent);

    }
}
