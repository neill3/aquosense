package com.dost.sil.aquosense;

/**
 * Created by Silerio on 12/4/2017.
 */

public class SerialData {

    private static String receivedSerialData;

    //create an object of SingleObject
    private static SerialData instance = new SerialData();

    private SerialData(){

    }

    public static SerialData getInstance(){
        return instance;
    }

    synchronized public void setSerialData(String data){
        this.receivedSerialData = data;
    }

    synchronized public String getSerialData(){
       return receivedSerialData;
    }

}
