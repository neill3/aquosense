package com.dost.sil.aquosense;

/**
 * Created by Silerio on 11/19/2017.
 */

public class DataLogs {

    private String id_dataLog;
    private String dname;
    private String date;
    private String gps;

    private boolean checkStatus;


    public DataLogs(String id, String dname, String date, String gps) {
        this.id_dataLog = id;
        this.dname = dname;
        this.date = date;
        this.gps = gps;
    }

    public boolean isCheckStatus() {
        return checkStatus;
    }

    public void setCheckStatus(boolean checkStatus) {
        this.checkStatus = checkStatus;
    }

    public String getId_dataLog() {
        return id_dataLog;
    }

    public void setId_dataLog(String id_dataLog) {
        this.id_dataLog = id_dataLog;
    }

    public String getDname() {
        return dname;
    }

    public void setDname(String dname) {
        this.dname = dname;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getGps() {
        return gps;
    }

    public void setGps(String gps) {
        this.gps = gps;
    }
}
