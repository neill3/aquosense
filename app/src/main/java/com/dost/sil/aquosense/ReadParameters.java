package com.dost.sil.aquosense;

/* Read Parameters using Serial Communication */


import android.Manifest;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.dost.sil.aquosense.UsbSerialService.MyBinder;



public class ReadParameters extends AppCompatActivity {
    IntentFilter filterUsbDisabled, filterUsbenable, filterObjectVerifier;
    ProgressBar spinner;
    Button begin_button, stop_button, save_button;
    TextView  devicename_textView, gps_textViewLat, gps_textViewLong, date_textView,  ph_textView,  do_textView, sal_textView, temp_textView, ec_textView, tds_textView, sg_textView, amm_textView;

    Mode mode = new Mode();
    boolean runAsync;

    GPSTracker gps;
    private static final int REQUEST_CODE_PERMISSION = 2;
    String mPermission = Manifest.permission.ACCESS_FINE_LOCATION;


    UsbSerialService usbService;
    boolean mUsbServiceBound = false;

    String currentDevice;


    // custom receiver when usb permission is okay
    private BroadcastReceiver usbAllowed = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            showToast("Data received!");
            begin_button.setEnabled(false);
            stop_button.setEnabled(true);
            save_button.setEnabled(true);
            sendRequest("NAME:", 1000);
            showList();


        }
    };

    //
    private BroadcastReceiver objectVerifier = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Toast.makeText(getBaseContext(), "Object Verified", Toast.LENGTH_SHORT).show();
        }
    };


    // custom receiver when usb is detached
    // need to re- work broadcast receivers
    private BroadcastReceiver usbDisabled = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
               stop_button.callOnClick();
               begin_button.setEnabled(true);
               stop_button.setEnabled(false);
             //  showToast("No USB");
               showAlertDialog("OTG/USB cable is not detected.");
        }
    };


    private void showAlertDialog(final String message){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(ReadParameters.this);
                builder.setTitle("WARNING");
                builder.setMessage(message);
                builder.create();
                builder.show();
            }
        });
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("READPARAMETER", "ONCREATE");

       // Toast.makeText(getApplicationContext(), "onCreate", Toast.LENGTH_SHORT).show();
        setContentView(R.layout.activity_read_parameters);

        runAsync = false;
        spinner = (ProgressBar) findViewById(R.id.progressbar_read);
        spinner.setVisibility(View.GONE);

        devicename_textView = (TextView) findViewById(R.id.textView_devicename);
        gps_textViewLat = (TextView) findViewById(R.id.textView_gpsLat);
        gps_textViewLong = (TextView) findViewById(R.id.textView_gpsLong);
        date_textView = (TextView) findViewById(R.id.textView_date);
        temp_textView = (TextView) findViewById(R.id.textView_TEMP);
        ph_textView = (TextView) findViewById(R.id.textView_PH);
        do_textView = (TextView) findViewById(R.id.textView_DO);
        sal_textView = (TextView) findViewById(R.id.textView_SAL);
        ec_textView = (TextView) findViewById(R.id.textView_COND);
        tds_textView = (TextView) findViewById(R.id.textView_TDS);
        sg_textView = (TextView) findViewById(R.id.textView_SG);
        amm_textView = (TextView) findViewById(R.id.textView_AMM);

        filterUsbDisabled = new IntentFilter("UsbSerialService.ACTION_USB_DISABLED");
        registerReceiver(usbDisabled, filterUsbDisabled);

        filterUsbenable = new IntentFilter("UsbSerialService.ACTION_USB_ENABLED");
        registerReceiver(usbAllowed, filterUsbenable);

        filterObjectVerifier = new IntentFilter("OBJECT.VERIFIED");
        registerReceiver(objectVerifier, filterObjectVerifier);


        //GPS
        try {
            if (ActivityCompat.checkSelfPermission(this, mPermission) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{mPermission},
                        REQUEST_CODE_PERMISSION);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        //Scan water parameter
        begin_button = (Button) findViewById(R.id.button_begin);
        begin_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mUsbServiceBound) {
                    showToast("Initializing sensors");
                    usbService.clearSerialData();
                    usbService.setUsbConnection();
                    spinner.setVisibility(View.VISIBLE);
                    begin_button.setEnabled(false);
                    stop_button.setEnabled(false);

                  // showList();

                } // end if service bound

            }

        }); // end scan_button listener


        //Stop scanning
        stop_button = (Button) findViewById(R.id.button_stop);
        stop_button.setEnabled(false);
        stop_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mUsbServiceBound) {
                    //task.cancel();

                    spinner.setVisibility(View.GONE);
                    // stopService(new Intent(getBaseContext(), UsbSerialService.class));

                    usbService.stopSerialReading();
                    stop_button.setEnabled(false);
                    begin_button.setEnabled(true);

                    runAsync = false;

                }
            }
        }); // end stop scans
        //Save data
        save_button = (Button) findViewById(R.id.button_save);
        save_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bundle passParameter =new Bundle();

                passParameter.putString("devicename", devicename_textView.getText().toString());
                String gps = "LAT: "+ gps_textViewLat.getText().toString() + ";" + "LONG: " + gps_textViewLong.getText().toString();
                passParameter.putString("gps", gps);
                passParameter.putString("date", currentDateTime());
                passParameter.putString("temp", temp_textView.getText().toString());
                passParameter.putString("ph",   ph_textView.getText().toString());
                passParameter.putString("do",   do_textView.getText().toString());
                passParameter.putString("ec",   ec_textView.getText().toString());
                passParameter.putString("sal",  sal_textView.getText().toString());
                passParameter.putString("tds",  tds_textView.getText().toString());
                passParameter.putString("sg",   sg_textView.getText().toString());
                passParameter.putString("amm",  amm_textView.getText().toString());
                passParameter.putInt("id", 00);

                Intent intent =  new Intent(v.getContext(), DisplayLog.class);
                intent.putExtras(passParameter);
                startActivity(intent);
            }
        });


    } // end onCreate


    @Override
    protected void onStart() {
        super.onStart();
        // added bound service
        Intent intent = new Intent(this, UsbSerialService.class);
        bindService(intent, rServiceConnection, Context.BIND_AUTO_CREATE);

    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        // resume bound service
        Intent intent = new Intent(this, UsbSerialService.class);
        bindService(intent, rServiceConnection, Context.BIND_AUTO_CREATE);

    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mUsbServiceBound) {
            unbindService(rServiceConnection);
            mUsbServiceBound = false;
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(usbAllowed);
        unregisterReceiver(usbDisabled);
        if(stop_button.isEnabled()) {
            stop_button.callOnClick();
        }

        if (mUsbServiceBound) {
            unbindService(rServiceConnection);
            mUsbServiceBound = false;
        }

       // Toast.makeText(getApplicationContext(), "DESTROY!", Toast.LENGTH_SHORT).show();

    }


    private ServiceConnection rServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            MyBinder binder = (MyBinder) service;
            usbService = binder.getService();
            mUsbServiceBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mUsbServiceBound = false;
        }
    };

    private void showList(){
        Handler h = new Handler();
        final AlertDialog.Builder listDialog = new AlertDialog.Builder(ReadParameters.this);

        try {

            h.postDelayed(new Runnable() {
                @Override
                public void run() {


                    // Mode mode = new Mode();
                    mode.list();
                    com.dost.sil.aquosense.List myList = com.dost.sil.aquosense.List.getInstance();
                    final String[] r = myList.getDevices();

                    if (mode.modeStat() == true) {
                        listDialog.setTitle("Select device:");
                        sendRequest("NSTA;X;0:", 2000);
                        listDialog.setItems(myList.getDevices(), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                showToast("you have selected device " + r[which].toString());
                                // sendRequest("NSTAT;" + r[which].toString() +";OK:");  // for updating, receiver is unablek to receive this handshake
                                sendRequest("READ;" + r[which].toString() + ":",3000);
                                runAsync = true;
                                currentDevice = r[which].toString();
                                gatherData();



                            }
                        });
                    } else {
                        showAlertDialog("Unable to receive data.");
                         sendRequest("STOP:", 1000);  // to be edited (handshake)
                     //   showToast("NSTAT:ERROR");

                    }

                    listDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {
                      //      showToast("No selected device");
                            stop_button.callOnClick();
                        }
                    });

                    listDialog.create();
                    listDialog.show();

                }
            }, 3000);

        }catch(Exception e){
            showToast(e.getMessage());
        }

    }


    private void showToast(String message){
        if(message!=null) {
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }
    }

    synchronized private void sendRequest(final String request, int time){

        if(mUsbServiceBound){

            final Handler h = new Handler();
            h.postDelayed(new Runnable() {
                @Override
                public void run() {
                    usbService.sendSerialtoBluno(request);
                }
            },time);

        }
    }


    private void gatherData(){

        try {
            if (runAsync == true) {

                Handler h = new Handler();
                h.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        AsyncTaskRunner runner = new AsyncTaskRunner();  // do not relocate this line
                        runner.execute();
                    }
                }, 5000);
            }
        }catch(Exception e){
            showToast(e.getMessage());
        }
    } // end gatherData


    // function to get current date time
    private String currentDateTime() {

        Date currentDate = new Date(System.currentTimeMillis());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String currentDateTime = sdf.format(currentDate);

        return currentDateTime;
    }
    private class AsyncTaskRunner extends AsyncTask<Void, String, Void> {
        Read r = Read.getInstance();
        @Override
        protected void onPostExecute(Void aVoid) {
         //   showToast("display data");
            try {

                if (mode.modeStat()){

                    sendRequest("RSTAT;" + currentDevice +";0:", 500);
              //      showToast("mode stat true");

                    //  showToast(r.getDevicename().toString());
                devicename_textView.setText(r.getDevicename().toString());
                date_textView.setText(r.getDate().toString());
                if (r.getDate().toString().equals("0"))
                    date_textView.setText(currentDateTime());
                temp_textView.setText(r.getTemp().toString());
                ph_textView.setText(r.getPh().toString());
                do_textView.setText(r.getdOx().toString());
                ec_textView.setText(r.getEc().toString());
                sal_textView.setText(r.getSal().toString());
                tds_textView.setText(r.getTds().toString());
                sg_textView.setText(r.getSg().toString());
                amm_textView.setText(r.getAmm().toString());

                gps = new GPSTracker(ReadParameters.this);

                // if gps is 0;
                if (gps.canGetLocation()) {

                    double latitude = gps.getLatitude();
                    double longitude = gps.getLongitude();
                    String lat = Double.toString(latitude);
                    String longitude1 = Double.toString(longitude);
                    gps_textViewLat.setText(lat);
                    gps_textViewLong.setText(longitude1);


                    // \n is for new line
                    //   Toast.makeText(getApplicationContext(), "Your Location is - \nLat: "
                    //           + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
                } else {
                    // can't get location
                    // GPS or Network is not enabled
                    // Ask user to enable GPS/network in settings
                    gps.showSettingsAlert();
                }

            }else{
                    sendRequest("RSTAT;" + currentDevice +";1:", 500);
                }

            }catch(Exception e){

            }

            // call for another set Data
            gatherData();


        }

        @Override
        protected Void doInBackground(Void... params) {

            try {
                if(mUsbServiceBound) {  // do not remove or it will fuck up
                    r.clear();  // clear Read data first
                    mode.read();

                    } //
            }
            catch(Exception e){

            }

            return null;
        } // end doInBackground

        @Override
        protected void onProgressUpdate(String... x) {
            Toast.makeText(getApplicationContext(), x[0],Toast.LENGTH_SHORT).show();

        }
    }

}
