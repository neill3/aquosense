package com.dost.sil.aquosense;

/**
 * Created by Silerio on 12/3/2017.
 */

public class ModeRead implements ModeState {


    private final Mode mode;
    private boolean modeStat;

    public ModeRead(Mode mode) {
        this.mode = mode;
    }


    @Override
    public boolean modeStat() {
        return this.modeStat;
    }

    @Override
    public void list() {
        mode.setState(mode.getList());
        mode.list();
    }

    @Override
    public void read() {

        try {

            SerialData sd = SerialData.getInstance();
            String dataSet = sd.getSerialData();

            int startDataSet = 0;

            //1st set of if else
            if (dataSet.contains("XREAD;")) {  // receiving header of bluno
                startDataSet = dataSet.lastIndexOf("XREAD;");
            } else {
                this.modeStat = false;
            } // end 1st set'


            int endDataSet = dataSet.lastIndexOf(":");
            int checkSumDataSet = dataSet.lastIndexOf("+");

            if (startDataSet < endDataSet) {

                char[] serialChar = null;

                try {
                    serialChar = dataSet.substring(startDataSet, checkSumDataSet).toCharArray();
                } catch (Exception e) {

                }

                // get current length of the dataSet
                int dSetLength = serialChar.length;
                int calculatedSum = 0;

                for (int counter = 0; counter < dSetLength; counter++) {

                    calculatedSum ^= serialChar[counter];

                } // end for Loop (int counter = 0; counter <chkSumDataSet ; counter++)

                String sumInSet = dataSet.substring(checkSumDataSet, endDataSet).replaceAll("[^0-9.]", "");

                int number;
                try {
                    number = Integer.parseInt(sumInSet);

                } catch (Exception e) {
                    number = 0;
                }
                int sumOfSet = number;

                if (sumOfSet == calculatedSum) {
                    //  sendSerialData to duino
                    //  sendSerialtoBluno("RSTAT,DEVNAME,RETURN STATUS;");
                    //  parseSerialData(dataSet ,startDataSet, endDataSet);

                    Read myReadings = Read.getInstance();
                    myReadings.parseSerialData(dataSet, startDataSet, endDataSet);

                    this.modeStat = true;

                } else {
                    // sendSerialtoBluno("RSTAT,DEVNAME,RETURN STATUS;");
                    // hmapSerial.put("devicename", "error");
                    // SERIALDATA = "";

                    this.modeStat = false;
                }

            } // end if
        }catch(Exception e){

        }

    }

    @Override
    public void calibrate() {
    }

    @Override
    public void fetch() {
        mode.setState(mode.getFetch());
        mode.fetch();
    }

    @Override
    public void files() {
        mode.setState(mode.getFiles());
        mode.files();

    }

}
