package com.dost.sil.aquosense;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class DisplayLog extends AppCompatActivity {
    private DBHelper mydb;

    Button save, cancel;

    TextView devicename_textViewLog, dt_textViewLog, gps_textViewLog, temp_textViewLog, ph_textViewLog, do_textViewLog, ec_textViewLog, sal_textViewLog, tds_textViewLog, sg_textViewLog, amm_textViewLog;
   // String [] parameter;
    int id_To_Update = 0;
    Bundle extras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_log);

        mydb = new DBHelper(this);

        devicename_textViewLog = (TextView) findViewById(R.id.textViewLog_devicename);
        gps_textViewLog = (TextView) findViewById(R.id.textViewLog_GPS);
        dt_textViewLog = (TextView) findViewById(R.id.textViewLog_date);
        temp_textViewLog = (TextView) findViewById(R.id.textViewLog_TEMP);
        ph_textViewLog = (TextView) findViewById(R.id.textViewLog_PH);
        do_textViewLog = (TextView) findViewById(R.id.textViewLog_DO);
        ec_textViewLog = (TextView) findViewById(R.id.textViewLog_EC);
        sal_textViewLog = (TextView) findViewById(R.id.textViewLog_SAL);
        tds_textViewLog = (TextView) findViewById(R.id.textViewLog_TDS);
        sg_textViewLog = (TextView) findViewById(R.id.textViewLog_SG);
        amm_textViewLog = (TextView) findViewById(R.id.textViewLog_AMM);

        save = (Button) findViewById(R.id.button_saveLog);
        cancel = (Button) findViewById(R.id.button_cancelLog);

        Intent intent = getIntent();
        extras = intent.getExtras();

        if (extras != null) {
            int id = extras.getInt("id");

            //   parameter = extras.getStringArray("parameter");


        // add new data
        if(id == 00) {

            devicename_textViewLog.setText(extras.getString("devicename"));

            String [] divide = extras.getString("gps").split(";");
            try {
                gps_textViewLog.setText(divide[0] + "\n" + divide[1]);
            }catch (Exception e){

            }

            dt_textViewLog.setText(extras.getString("date"));
            temp_textViewLog.setText(extras.getString("temp") + " (°C)");
            ph_textViewLog.setText(extras.getString("ph"));
            do_textViewLog.setText(extras.getString("do"));
            ec_textViewLog.setText(extras.getString("ec") + " (µS/cm)");
            sal_textViewLog.setText(extras.getString("sal") + " (ppt)");
            tds_textViewLog.setText(extras.getString("tds") + " (mg/L)");
            sg_textViewLog.setText(extras.getString("sg") + " (Kg/m³)");
            amm_textViewLog.setText(extras.getString("amm"));

        }


            // data select from logs

            if (id > 0) {
                //means this is the view part not the add data part.
                save.setVisibility(View.INVISIBLE);
                cancel.setVisibility(View.INVISIBLE);

                Cursor rs = mydb.getData(id);
                id_To_Update = id;
                rs.moveToFirst();

                //   String dt = rs.getString(rs.getColumnIndex(DBHelper.PARAMETER_DATE));
                String dname = rs.getString(rs.getColumnIndex(DBHelper.PARAMETER_DEVICENAME));
                String gps = rs.getString(rs.getColumnIndex(DBHelper.PARAMETER_GPS));
                String date = rs.getString(rs.getColumnIndex(DBHelper.PARAMETER_DATE));
                String ph = rs.getString(rs.getColumnIndex(DBHelper.PARAMETER_PH));
                String dox = rs.getString(rs.getColumnIndex(DBHelper.PARAMETER_DO));
                String sal = rs.getString(rs.getColumnIndex(DBHelper.PARAMETER_SAL)) + " (ppt)";
                String temp = rs.getString(rs.getColumnIndex(DBHelper.PARAMETER_TEMP)) + " (°C)";
                String cond = rs.getString(rs.getColumnIndex(DBHelper.PARAMETER_EC)) + " (µS/cm)";
                String tds = rs.getString(rs.getColumnIndex(DBHelper.PARAMETER_TDS)) + " (mg/L)";
                String sg = rs.getString(rs.getColumnIndex(DBHelper.PARAMETER_SG)) + " (Kg/m³)";
                String amm = rs.getString(rs.getColumnIndex(DBHelper.PARAMETER_AMM));


                if (!rs.isClosed()) {
                    rs.close();
                }

                devicename_textViewLog.setText(dname);

                String [] divide = gps.split(";");
                try {
                    gps_textViewLog.setText(divide[0] + "\n" + divide[1]);
                }catch (Exception e){

                }


                dt_textViewLog.setText(date);
                ph_textViewLog.setText(ph);
                do_textViewLog.setText(dox);
                sal_textViewLog.setText(sal);
                temp_textViewLog.setText(temp);
                ec_textViewLog.setText(cond);
                tds_textViewLog.setText(tds);
                sg_textViewLog.setText(sg);
                amm_textViewLog.setText(amm);

            }

        } // end onCreate
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.displaylog_delete, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.delete_data:

                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Delete log data")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                mydb.deleteLog(id_To_Update);
                                Toast.makeText(getApplicationContext(), "Deleted Successfully",
                                        Toast.LENGTH_SHORT).show();
                                finish();
                                Intent intent = new Intent(getApplicationContext(),ViewLogs.class);
                                startActivity(intent);
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // User cancelled the dialog
                            }
                        });

                AlertDialog d = builder.create();
                d.setTitle("Are you sure");
                d.show();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }



    public void save(View view){

       // mydb.insertParameter(parameter[0],parameter[1],parameter[2],parameter[3],parameter[4],parameter[5],parameter[6], parameter[7]);

        mydb.insertParameter(extras.getString("devicename"), extras.getString("date"), extras.getString("gps"), extras.getString("temp"), extras.getString("ph"),extras.getString("do"), extras.getString("ec"), extras.getString("sal"), extras.getString("tds"), extras.getString("sg"), extras.getString("amm"));
        Toast.makeText(getApplicationContext(), "Data log created",Toast.LENGTH_SHORT).show();
        finish();

    }

    public void cancel(View view){
        finish();
    }
}
