package com.dost.sil.aquosense;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Silerio on 11/19/2017.
 */

public class DataListAdapter extends ArrayAdapter<DataLogs>{

    private Context mContext;
    int mResource;
    TextView tvdName, tvDate, tvGPS;
    CheckBox checkBox;
    ArrayList<DataLogs> items;  // added 1/12/2018



    public DataListAdapter(Context context, int resource, ArrayList<DataLogs> objects) {
        super(context, resource, objects);
        mContext = context;
        mResource = resource;
        items = objects;
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        String id_data = getItem(position).getId_dataLog();
        String dname = getItem(position).getDname();
        String date  = getItem(position).getDate();
        String gps = getItem(position).getGps();

        DataLogs datalogs = new DataLogs(id_data, dname,date, gps);
        LayoutInflater inflater = LayoutInflater.from(mContext);
        convertView = inflater.inflate(mResource, parent, false);

        tvdName = (TextView) convertView.findViewById(R.id.textView1);
        tvDate = (TextView) convertView.findViewById(R.id.textView2);
        tvGPS= (TextView) convertView.findViewById(R.id.textView3);
        checkBox = (CheckBox) convertView.findViewById(R.id.checkbox_datalog);

        tvdName.setText(dname);
        tvDate.setText(date);
        tvGPS.setText(gps);

        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                DataLogs dL = (DataLogs) items.get(position);
                if(isChecked){

                    dL.setCheckStatus(true);
                }else{
                    dL.setCheckStatus(false);

                }
            }
        });


        tvDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DataLogs dL = (DataLogs) items.get(position);
                int id_To_Search = Integer.parseInt(dL.getId_dataLog().toString());
                Bundle dataBundle = new Bundle();
                dataBundle.putInt("id", id_To_Search);

                Intent intent = new Intent(mContext,DisplayLog.class);

                intent.putExtras(dataBundle);
                mContext.startActivity(intent);

            }
        });




        return convertView;

    }

    public TextView getTvdName() {
        return tvdName;
    }

    public void setTvdName(TextView tvdName) {
        this.tvdName = tvdName;
    }

    public TextView getTvDate() {
        return tvDate;
    }

    public void setTvDate(TextView tvDate) {
        this.tvDate = tvDate;
    }

    public TextView getTvGPS() {
        return tvGPS;
    }

    public void setTvGPS(TextView tvGPS) {
        this.tvGPS = tvGPS;
    }


}
