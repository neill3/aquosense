package com.dost.sil.aquosense;

/**
 * Created by Silerio on 12/3/2017.
 */

public interface ModeState {

    public boolean modeStat();
    public void list();
    public void read();
    public void calibrate();
    public void fetch();
    public void files();

}
