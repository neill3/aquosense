package com.dost.sil.aquosense;


/* Typical flow
    onOptionsMenu -> fetch - > sendlist to bluno -> show list through alert dialog ->
    select name -> prompt selected date -> send DevName and date to bluno ->
*/

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;

public class ViewLogs extends AppCompatActivity{

    ProgressDialog progressDialog;
//
    final String TAG = "VIEWLOGS";

    // broadcast receivers for usb connection
    IntentFilter filterUsbDisabled, filterUsbenable;

   // private ListView obj;

    ArrayList array_list_id;

    //service bound
    UsbSerialService usbService;
    boolean mUsbServiceBound = false;
    DataListAdapter adapter;

    ListView mlistView;

    // invisible editText
    public static EditText eT;

    Mode mode = new Mode();
    boolean runAsync;
    String currentDevice;

    private DBHelper mydb;

    public  static String selectedDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_logs);

        mydb = new DBHelper(this);

        eT = (EditText) findViewById(R.id.viewLog_editText);

        /* ListView adapter */
        mlistView= (ListView) findViewById(R.id.listView_logs);

        mydb = new DBHelper(this);
        array_list_id = mydb.getAllParameterLogs();

      //  DataLogs data[] = new DataLogs[array_list_id.size()];
        ArrayList<DataLogs> dataLogs = new ArrayList();
        Log.d("ViewLogs", "entering loop");

        for(int x = 0; x< array_list_id.size(); x++){
            String id = array_list_id.get(x).toString();

            Cursor rs = mydb.getData(Integer.parseInt(id));
            rs.moveToFirst();

            //   String dt = rs.getString(rs.getColumnIndex(DBHelper.PARAMETER_DATE));
            String id_data  = rs.getString(rs.getColumnIndex(DBHelper.PARAMETER_ID));
            String dname =  rs.getString(rs.getColumnIndex(DBHelper.PARAMETER_DEVICENAME));
            String date = rs.getString(rs.getColumnIndex(DBHelper.PARAMETER_DATE));
            String gps = rs.getString(rs.getColumnIndex(DBHelper.PARAMETER_GPS));

            dataLogs.add(new DataLogs(id_data, dname, date, gps));
        }


       // mlistView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
       // mlistView.setItemsCanFocus(false);

        adapter = new DataListAdapter(this, R.layout.adapter_view_layout, dataLogs);
        mlistView.setAdapter(adapter);
        mlistView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

        /* end data adapter */


        /*broadcast receivers*/
        filterUsbDisabled = new IntentFilter("UsbSerialService.ACTION_USB_DISABLED");
        registerReceiver(usbDisabled, filterUsbDisabled);

        filterUsbenable = new IntentFilter("UsbSerialService.ACTION_USB_ENABLED");
        registerReceiver(usbAllowed, filterUsbenable);
        // end broadcast receivers

        mlistView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                showToast("cliocking");
                //int id_To_Search = position + 1;
                int id_To_Search = Integer.parseInt(array_list_id.get(position).toString());
                showToast(Integer.toString(id_To_Search) + " search");
                Bundle dataBundle = new Bundle();
                dataBundle.putInt("id", id_To_Search);

                Intent intent = new Intent(getApplicationContext(),DisplayLog.class);

                intent.putExtras(dataBundle);
                startActivity(intent);

            }
        });

        eT.setVisibility(View.INVISIBLE);
        eT.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                showToast(s.toString());
                //
                sendRequest("FILE;" + currentDevice + ";"+ s.toString() + ":", 1000);
                // open Progress Dialog


            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    } // end onCreate

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.viewlogs_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.fetch_data:
                fetchData();
                return true;
            case R.id.delete_dataLog:
                deleteLogs();
                return true;
            case R.id.time_picker:
                adjustTimeInterval();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }



    @Override
    protected void onStart() {
        super.onStart();
        // added bound service
        Intent intent = new Intent(ViewLogs.this, UsbSerialService.class);
        bindService(intent, rServiceConnection, Context.BIND_AUTO_CREATE);

    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        // resume bound service
        Intent intent = new Intent(ViewLogs.this, UsbSerialService.class);
        bindService(intent, rServiceConnection, Context.BIND_AUTO_CREATE);

    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mUsbServiceBound) {
            unbindService(rServiceConnection);
            mUsbServiceBound = false;
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        unregisterReceiver(usbAllowed);
        unregisterReceiver(usbDisabled);

        if (mUsbServiceBound) {
            usbService.stopSerialReading();
            unbindService(rServiceConnection);
            mUsbServiceBound = false;
        }

    }

    /* for service bound */
    private ServiceConnection rServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            UsbSerialService.MyBinder binder = (UsbSerialService.MyBinder) service;
            usbService = binder.getService();
            mUsbServiceBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mUsbServiceBound = false;
        }
    };

    public void showToast(String message){
        try {
            Toast.makeText(this.getApplicationContext(), message, Toast.LENGTH_SHORT).show();
        }catch(Exception e){

        }

    }

    /*Receiver for usb disabled */
    private BroadcastReceiver usbDisabled = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            showToast("No USB");
        }
    };

    /*Receiver for usb enabled */
    private BroadcastReceiver usbAllowed = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            showToast("Requesting data..");
            sendRequest("NAME:", 1000);
            showList();


        }
    };

    /*delete log */
    /*completed*/
    private void deleteLogs(){

        int checkCounter = 0;

        for(int x = (mlistView.getCount()-1); x >= 0 ; x--){

            if(adapter.getItem(x).isCheckStatus()){
                int id_To_Delete = Integer.parseInt(array_list_id.get(x).toString());
                mydb.deleteLog(id_To_Delete);
                checkCounter++;
            }
        }
        if(checkCounter == 0) {
            showToast("Select an item to be deleted.");
        }else{
            showToast("Successfully deleted logs");
            finish();
            Intent intent = new Intent(this,ViewLogs.class);
            startActivity(intent);

            //restart activity
        }

    }// delete log


    private void displayDatePicker(){
          /*display date Picker*/
        DialogFragment newFragment = new DatePickerFragment();
        FragmentManager fm = this.getFragmentManager();
        newFragment.show(fm,"datePicker");
    }


    /* start of fetching data */
    private void fetchData(){

        AsyncTaskRunner runner = new AsyncTaskRunner();  // do not relocate this line
        runner.execute();
        /*
        if(mUsbServiceBound){
            usbService.clearSerialData();
            usbService.setUsbConnection();
        }
        */

    }


    private void showList(){
        Handler h = new Handler();
        final AlertDialog.Builder listDialog = new AlertDialog.Builder(ViewLogs.this);

        try {

            h.postDelayed(new Runnable() {
                @Override
                public void run() {


                    // Mode mode = new Mode();
                    mode.list();
                    com.dost.sil.aquosense.List myList = com.dost.sil.aquosense.List.getInstance();
                    final String[] r = myList.getDevices();

                    if (mode.modeStat() == true) {
                        listDialog.setTitle("Select device:");
                        sendRequest("NSTA;X;0:", 2000);
                        listDialog.setItems(myList.getDevices(), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                showToast("you have selected device " + r[which].toString());
                                // sendRequest("NSTAT;" + r[which].toString() +";OK:");  // for updating, receiver is unablek to receive this handshake
                               // sendRequest("READ;" + r[which].toString() + ":",3000);
                                runAsync = true;
                                currentDevice = r[which].toString();
                                displayDatePicker();
                             //   gatherData();



                            }
                        });
                    } else {
                        showAlertDialog("Unable to receive data.");
                        sendRequest("STOP:", 1000);  // to be edited (handshake)
                        //   showToast("NSTAT:ERROR");

                    }

                    listDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {
                            // showToast("No selected device");
                            // stop_button.callOnClick();
                            // stop_fetching_data
                        }
                    });

                    listDialog.create();
                    listDialog.show();

                }
            }, 3000);

        }catch(Exception e){
            showToast(e.getMessage());
        }

    }


    synchronized private void sendRequest(final String request, int time){

        if(mUsbServiceBound){

            final Handler h = new Handler();
            h.postDelayed(new Runnable() {
                @Override
                public void run() {
                    usbService.sendSerialtoBluno(request);
                }
            },time);

        }
    }

    private void showAlertDialog(final String message){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(ViewLogs.this);
                builder.setTitle("WARNING");
                builder.setMessage(message);
                builder.create();
                builder.show();
            }
        });
    }


    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        public static String setDateDate;

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it

            DatePickerDialog datePicker = new DatePickerDialog(getActivity(),R.style.datepicker, this, year, month, day);
            datePicker.setTitle("Select date ");
            // return the DatePickerDialog
            return datePicker;

        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            //format year-month-day
            String setDate = Integer.toString(year) + "-" + Integer.toString(month) + "-" + Integer.toString(day);
            Log.i("ViewLogs", "Selected date:" + setDate);
         //   ViewLogs.selectedDate = "";
         //   ViewLogs.selectedDate = setDate;

            setDateDate = setDate;
            eT.setText(setDate);

        }


    } // end DatePickerFragment

    int checkedItem = 0;
    private void adjustTimeInterval() {

        final String [] time = {"5 minutes", "10 minutes", "20 minutes", "30 minutes", "1 hour", "2 hours", "4 hours", "6 hours", "12 hours"};


        AlertDialog.Builder timepick = new AlertDialog.Builder(ViewLogs.this);

        timepick.setTitle("Adjust save interval");
        timepick.setSingleChoiceItems(time, 3 , new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                checkedItem = which;
            }
        });
        timepick.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getApplicationContext(), Integer.toString(checkedItem), Toast.LENGTH_SHORT).show();
            }
        });
        timepick.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        timepick.create();
        timepick.show();



            /*

            timepick.setView(dialogView);
            timepick.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });


            NumberPicker hour = (NumberPicker) dialogView.findViewById(R.id.hour_pick);
            NumberPicker minute = (NumberPicker) dialogView.findViewById(R.id.minute_pick);

            hour.setMinValue(0);
            hour.setMaxValue(12);
            minute.setMinValue(0);
            minute.setMaxValue(60);

            AlertDialog time = timepick.create();
            time.show();

            */


    }





    private class AsyncTaskRunner extends AsyncTask<String, String, Boolean> {


        @Override
        protected void onPreExecute() {

            progressDialog = new ProgressDialog(ViewLogs.this);
            progressDialog.setMax(100); // Progress Dialog Max Value
            progressDialog.setMessage("transferring files..."); // Setting Message
            progressDialog.setTitle("Updating data"); // Setting Title
            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL); // Progress Dialog Style Horizontal
            // Display Progress Dialog
            progressDialog.setCancelable(false);
            progressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(Boolean status) {
            if (status){
                Toast.makeText(ViewLogs.this, "Success", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }else{
                Toast.makeText(ViewLogs.this, "NO AVAILABLE DATA", Toast.LENGTH_SHORT).show();
               // progressDialog.setMessage("Stored data is unavailable for this date");
                progressDialog.dismiss();

            }

            finish();
            Intent updateLogs = new Intent(getApplicationContext(), ViewLogs.class);
            startActivity(updateLogs);

        }

        @Override
        protected Boolean doInBackground(String... params) {

            try {
                // get number of filenames
                Fetch fetch = Fetch.getInstance();

                // initialize file
                FileName file = FileName.getInstance();
                // f.clear();  // take note to add
                mode.files();

                if (mode.modeStat()) {

                    if (file.getFilenames().length <= 0) {
                        return false;
                    }
                    int incre = 100 / file.getFilenames().length;

                    for (int x = 0; x < file.getFilenames().length; x++) {
                        String filename = file.getFilenames()[x].toString();
                        // check database
                        if (mydb.checkIfDateExists(filename)) {
                            publishProgress(file.getFilenames()[x].toString());
                            continue;
                        }

                        fetch.clear();
                        mode.fetch();
                        if (mode.modeStat()) {


                            mydb.insertParameter(fetch.getDevicename(), fetch.getDate(), fetch.getGps(), fetch.getTemp(), fetch.getPh(), fetch.getdOx(), fetch.getEc(), fetch.getSal(), fetch.getTds(), fetch.getSg(), fetch.getAmm());
                            publishProgress(Integer.toString(incre));

                        }

                        Thread.sleep(1000);
                    }

                    publishProgress("100");
                }// end if mode.modeStat(files)

            }catch(Exception e){

            }



            /*
            // thread..
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        while (progressDialog.getProgress() <= progressDialog.getMax()) {
                            Thread.sleep(200);
                            publishProgress("");
                            if (progressDialog.getProgress() == progressDialog.getMax()) {
                                progressDialog.dismiss();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }).start();
            */


            return true;
        } // end doInBackground

        @Override
        protected void onProgressUpdate(String... x) {
            //   Toast.makeText(getApplicationContext(), x[0],Toast.LENGTH_SHORT).show();
         //   progressDialog.incrementProgressBy(Integer.parseInt(x[0]));
         //   progressDialog.incrementProgressBy(2);
            if(x.equals("100")){
                progressDialog.setProgress(100);
            }else {
                progressDialog.incrementProgressBy(Integer.parseInt(x[0]));
            }

        }
    }



}
